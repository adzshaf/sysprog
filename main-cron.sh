#!/bin/bash
conn="Wired connection 1"
today=$(TZ=":Asia/Jakarta" date)
file=$(pwd)"/air_quality.txt"


function print_newline(){
    echo -e '\n'
}

function enable_connection() {
    echo "You don't have active connection"
    print_newline
    echo "Here are your available connections"
    nmcli con show

    print_newline
    echo "Choose your connection: " $conn

    print_newline
    nmcli con up "$conn" 

    print_newline
    echo "Your connection is active"
    nmcli con show --active
}

function available_connection(){
    print_newline
    echo "Here are your active connections"
    nmcli con show --active

    if [[ -z $(nmcli con show --active | tail -n 1 | grep UUID) ]]; then
        print_newline
	echo "Choose your active connection: " $conn
    else
        enable_connection
    fi
}

available_connection

API_KEY="982e8f5c-163c-41f9-b857-7269b2a35642"
response=$(curl 'http://api.airvisual.com/v2/nearest_city?key='$API_KEY)
aqius=$(echo $response | jq '. | .data | .current | .pollution | .aqius')

echo "this is aqius $aqius"

if [[ $aqius -lt 100 ]]
then
    print_newline
    speaker-test -t sine -f 1000 -l 1
    
    print_newline
    echo "[$today] The air quality is not bad, with the AQI of $aqius" >> $file
    
    print_newline
    cat $file | tail -n 1

    print_newline
    nmcli c m "$conn" 802-3-ethernet.auto-negotiate yes 

    print_newline
    echo "AutoNeg is enabled"
    
    nmcli c s "$conn" | grep 802-3-ethernet.auto-negotiate

else
    print_newline
    speaker-test -t sine -f 4000 -l 1

    print_newline
    echo "[$today} The air is polluted, with the AQI of $aqius" >> $file

    print_newline
    cat $file | tail -n 1 

    nmcli con down "$conn" 
fi
