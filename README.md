# TK Sysprog

## AirVisual API-Sound-E1000

- Nurul Srianda Putri (1806205602)
- Shafiya Ayu Adzhani (1806235845)
- Kirana Alfatianisa (1806186843)

## Requirements

- `sudo apt-get install alsa-utils`

- `sudo apt-get install jq`

## Run this script

```bash
bash main.sh
```

## To run script as bootscript

It is better if you run as a root (to avoid permission denied)

- `cp run_main.sh /usr/bin/`

- `cp main.sh /usr/bin/`

- `cp aqi_level.service /lib/systemd/system`

- `systemctl start aqi_level.service` (to check if it works)

To run everytime it is booted:

- `systemctl daemon-reload`

- `systemctl enable aqi_level.service`
