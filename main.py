from subprocess import call
import requests
import time

# enable ethernet again
call(["sudo", "modprobe", "e1000", "AutoNeg=1", "RxIntDelay=1000000"])

API_KEY = '982e8f5c-163c-41f9-b857-7269b2a35642'
response = requests.get('http://api.airvisual.com/v2/nearest_city?key=' + API_KEY)

aqi_level = response.json()['data']['current']['pollution']['aqius']

if aqi_level < 100:
    print("Pollution is not bad")
    call(["speaker-test", "-t", "sine", "-f", "1000", "-l", "1"])
else:
    # if it is bad, remove e1000
    print("Pollution is bad") 
    call(["speaker-test", "-t", "sine", "-f", "4000", "-l", "1"])
    call(["sudo", "rmmod", "e1000"])